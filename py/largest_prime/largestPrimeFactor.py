class LargestPrimeFactor():
		"""
		Returns all the prime factors of a positive integer in a list. 
		Raise a ValueError in case of wrong input.

		Note:
		 - When n is a prime number, the prime factorization is just n itself
		 - The number 1 is called a unit. It has no prime factors and is neither prime nor composite.

		ref. https://en.wikipedia.org/wiki/Table_of_prime_factors
		"""
