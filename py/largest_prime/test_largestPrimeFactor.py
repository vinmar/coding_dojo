import unittest
from largestPrimeFactor import *

"""
unittest assertions
------------------
assertEqual
assertRaises:
  with self.assertRaises(ValueError):
    self.largestPrimeFactor.compute(...)
"""

class LargestPrimeFactorTest(unittest.TestCase):
	def setUp(self):
		self.largestPrimeFactor = LargestPrimeFactor()

	"""
	Exampe of first test 
	"""
	def test_method_is_implemented(self):
		self.assertEqual(hasattr(self.largestPrimeFactor, 'compute'), True)


if __name__ == '__main__':
    unittest.main()