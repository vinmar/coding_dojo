import unittest
from stringCalculator import *

"""
unittest assertions
------------------
assertEqual
assertRaises:
  with self.assertRaises(ValueError):
    self.stringCalculator.add(...)
"""

class StringCalculatorTest(unittest.TestCase):
	def setUp(self):
		self.stringCalculator = StringCalculator()

	"""
	Exampe of first test 
	"""
	def test_method_is_implemented(self):
		self.assertEqual(hasattr(self.stringCalculator, 'add'), True)


if __name__ == '__main__':
    unittest.main()